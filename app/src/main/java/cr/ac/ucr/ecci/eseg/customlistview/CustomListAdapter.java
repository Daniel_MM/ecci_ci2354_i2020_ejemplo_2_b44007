package cr.ac.ucr.ecci.eseg.customlistview;

import androidx.annotation.NonNull;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;
    private final String[] itemdescription;
    CustomListAdapter(Activity context, String[] itemname, Integer[] imgid, String[] itemdescription) {
        super(context, R.layout.my_custom_list, itemname);
        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
        this.itemdescription = itemdescription;
    }
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.my_custom_list, null, true);
        TextView nombre = (TextView) rowView.findViewById(R.id.name);
        ImageView imagen = (ImageView) rowView.findViewById(R.id.icon);
        TextView descripcion = (TextView) rowView.findViewById(R.id.description);
        nombre.setText(itemname[position]);
        imagen.setImageResource(imgid[position]);
        descripcion.setText(itemdescription[position]);
        return rowView;
    }
}